package Cajero.Modelos;

public class Cuenta {
    private String id;
    private float saldo;
    private int clave;
    private Usuario usuario;

    public Cuenta(String id, int clave, Usuario usuario) {
        this.id = id;
        this.clave = clave;
        this.usuario = usuario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
