package Cajero.Controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import java.io.IOException;

public class ctrlPrincipalController {
    @FXML
    private BorderPane ctrlPrincipal;

    public void initialize(){
        AnchorPane MenuPrincipal = null;
        try {
            MenuPrincipal = FXMLLoader.load (getClass().getResource("../View/MenuPrincipal.fxml"));
            this.ctrlPrincipal.setCenter(MenuPrincipal);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
